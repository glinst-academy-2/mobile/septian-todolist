
// const form = document.querySelector('form');
// const ul = document.querySelector('ul');
// const button = document.querySelector('button');
// const input = document.getElementById('item');
// let itemsArray = localStorage.getItem('items') ? JSON.parse(localStorage.getItem('items')) : [];

// localStorage.setItem('items', JSON.stringify(itemsArray));
// const data = JSON.parse(localStorage.getItem('items'));

// const liMaker = (text) => {
//   const li = document.createElement('li');
//   li.textContent = text;
//   ul.appendChild(li);
// }

// form.addEventListener('submit', function (e) {
//   e.preventDefault();

//   itemsArray.push(input.value);
//   localStorage.setItem('items', JSON.stringify(itemsArray));
//   liMaker(input.value);
//   input.value = "";
// });

// data.forEach(item => {
//   liMaker(item);
// });

// button.addEventListener('click', function () {
//   localStorage.clear();
//   while (ul.firstChild) {
//     ul.removeChild(ul.firstChild);
//   }
//   itemsArray = [];
// });

document.addEventListener('DOMContentLoaded',function(){

    
        let holder = document.querySelector('.holder'),
            addBtn = document.querySelector('#add'),
            removeBtn = document.querySelector('#remove'),
            resultBox = document.querySelector('.results');

        let input = document.getElementsByTagName('input')[0];
        
        function setAttributes(element,attrs){
            for(let i in attrs){
                element.setAttribute(i,attrs[i]);
            }
        }

        setAttributes(input,{
            
            'name' : 'todo'
        });

        function addtoBtn(){

            addBtn.addEventListener('click',function(event){

            if(input.value !== ''){
                let openingDiv = '<div class="todo"><span>';
                let closingDiv = '</span><button class="edit">Edit</button><button class="save">Save</button><button class="removeBtn">Remove</button></div>';

               
                resultBox.innerHTML += openingDiv + input.value + closingDiv;
                input.value = '';
                event.preventDefault(); 
                


            } else{
                alert('Enter To-do!');
            }

            let innerBtn = document.querySelectorAll('.removeBtn');
            let editBtn = document.querySelectorAll('.edit');
            let saveBtn = document.querySelectorAll('.save');

            for(let collection = 0 ; collection < saveBtn.length ; collection++){
                saveBtn[collection].style.display = "none";
            }

            function removeToDo(){

                this.parentNode.style.display = 'none';
            }

            for(let k = 0 ; k < innerBtn.length ; k++){
                innerBtn[k].addEventListener('click',removeToDo);
            }

            function startContentEdit(){
                this.previousSibling.contentEditable = true;
                this.previousSibling.style.padding = "10px";
                this.previousSibling.style.boxShadow = "0 0 15px #fff";
                this.previousSibling.style.borderRadius = "10px";
                this.previousSibling.style.transition = "all .4s";

                this.style.display = "none";

                for(let el = 0 ; el < saveBtn.length ; el++){
                    this.nextSibling.style.display = 'inline-block';
                }
            }

            for(let j = 0 ; j < editBtn.length ; j++){
                editBtn[j].addEventListener('click',startContentEdit);
            }

            function saveContentEdit(){
                this.style.display = 'none';

                for(let j = 0 ; j < editBtn.length ; j++){

                    this.previousSibling.style.display = "inline-block";

                    this.parentNode.firstElementChild.style.display = "block";
                    this.parentNode.firstElementChild.contentEditable = false;
                    this.parentNode.firstElementChild.style.padding = "0px";
                    this.parentNode.firstElementChild.style.boxShadow = "0 0 0";
                }
            }

            for(let x = 0 ; x < saveBtn.length ; x++){
                saveBtn[x].addEventListener('click',saveContentEdit);
            }

            }); 
        }

        addtoBtn();
});