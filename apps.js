// let list = [];
// let listelement = document.getElementById('list');
// let input = document.getElementById('input');
// let button = document.getElementsByClassName('button')[0];

// button.addEventListener('click', function(event){
//     let value = input.value;
//     let el = "";
//     if(value){
//         list.push(value);

//         if(list.length){
//             for(let i =0; i< list.length; i++){
//                 el += "<li>"+list[i]+"</li>";
//             }
//             listelement.innerHTML = el;
//         }
//         console.log(list);
//     }

    
// });



var app = new function() {
  this.el = document.getElementById('lists');
  this.lists = JSON.parse(localStorage.getItem('lists')) ? JSON.parse(localStorage.getItem('lists')) : [];
  this.Count = function(data) {
    var el   = document.getElementById('counter');
    var name = 'list';
    if (data) {
      if (data > 1) {
        name = 'lists';
      }
      el.innerHTML = data + ' ' + name ;
    } else {
      el.innerHTML = 'No ' + name;
    }
  };
  
  this.FetchAll = function() {
    
    let data = ''
    if (this.lists.length >= 0) {
      for (i = 0; i < this.lists.length; i++) {
        data += '<tr>';
        data += '<td>' + this.lists[i] + '</td>';
        data += '<td><button onclick="app.Edit(' + i + ')">Edit</button></td>';
        data += '<td><button onclick="app.Delete(' + i + ')">Delete</button></td>';
        data += '</tr>';
      }
    }
    this.Count(this.lists.length);
    return this.el.innerHTML = data;
  };
  this.Add = function () {
    el = document.getElementById('add-name');
    // Get the value
    var list = el.value;
    if (list) {
      // Add the new value
      this.lists.push(list);
      // Reset input value
      el.value = '';
      localStorage.setItem("lists", JSON.stringify(this.lists));

      // Dislay the new list
      this.FetchAll();
      console.log(this.lists)
    }
  };
  this.Edit = function (item) {
    var el = document.getElementById('edit-name');
    // Display value in the field
    el.value = this.lists[item];
    // Display fields
    document.getElementById('spoiler').style.display = 'block';
    self = this;
    document.getElementById('saveEdit').onsubmit = function() {
      // Get value
      var list = el.value;
      if (list) {
        // Edit value
        self.lists.splice(item, 1, list.trim());
        localStorage.setItem("lists", JSON.stringify(self.lists));
        // Display the new list
        self.FetchAll();
        // Hide fields
        CloseInput();
      }
    }
  };
  this.Delete = function (item) {
    // Delete the current row
    this.lists.splice(item, 1);
    localStorage.setItem("lists", JSON.stringify(this.lists));
    // Display the new list
    this.FetchAll();
  };
  
}
app.FetchAll();
function CloseInput() {
  document.getElementById('spoiler').style.display = 'none';
}