$(function(){
    var operacao = "A"; 
    var indice_selecionado = -1; 
    var tbClientes = localStorage.getItem("tbClientes"); 
    tbClientes = JSON.parse(tbClientes);

		if(tbClientes == null){ 
	    tbClientes = [];
		}

		$("#frmCadastro").on("submit",function(){
			if(operacao == "A"){
			    return Adicionar(tbClientes);
			}else{
			    return Editar(tbClientes,indice_selecionado);
			}
		});

		Listar(tbClientes);

		$("#tblListar").on("click", ".btnEditar", function(){
	    operacao = "E";
	    indice_selecionado = parseInt($(this).attr("alt"));
			var cli = JSON.parse(tbClientes[indice_selecionado]);
	    $("#txtName").val(cli.Codigo);
		  $("#txtName").focus();
		});

		$("#tblListar").on("click", ".btnExcluir",function(){
	    indice_selecionado = parseInt($(this).attr("alt"));
			Excluir(tbClientes, indice_selecionado);
	    Listar(tbClientes);
		});
});

function Adicionar(tbClientes){

		var cliente = JSON.stringify({
       
        Name     : $("#txtName").val()
        
    });
    tbClientes.push(cliente);
		console.log("tbClientes - " + tbClientes);
    localStorage.setItem("tbClientes", JSON.stringify(tbClientes));
    alert("Registro adicionado.");
    return true;
}

function Editar(tbClientes,indice_selecionado){
    tbClientes[indice_selecionado] = JSON.stringify({
            
            Name     : $("#txtName").val()
            
        });
    localStorage.setItem("tbClientes", JSON.stringify(tbClientes));
    alert("Edited information.")
    operacao = "A"; 
    return true;
}

function Excluir(tbClientes, indice_selecionado){
    tbClientes.splice(indice_selecionado, 1);
    localStorage.setItem("tbClientes", JSON.stringify(tbClientes));
    alert("Record deleted.");

}

function Listar(tbClientes){
    $("#tblListar").html("");
    $("#tblListar").html(
        "<thead>"+
        "   <tr>"+
        "   <th></th>"+
        "   <th>Name</th>"+
        "   </tr>"+
        "</thead>"+
        "<tbody>"+
        "</tbody>"
        );
    for(var i in tbClientes){
        var cli = JSON.parse(tbClientes[i]);
        $("#tblListar tbody").append("<tr>");
        $("#tblListar tbody").append("<td><img src='localStorage/edit.png' alt='"+i+"'class='btnEditar'/><img src='localStorage/delete.png' alt='"+i+"' class='btnExcluir'/></td>");
        $("#tblListar tbody").append("<td>"+cli.Name+"</td>");
        $("#tblListar tbody").append("</tr>");
    }
}
